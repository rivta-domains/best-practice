# Gemensamma informationskomponenter

I tjänstekontraktsbeskrivningarna används ett antal komponenter som är återkommande inom flera tjänstedomäner eller tjänstekontrakt. Dessa komponenter finns dokumenterade här.

Informationsobjekt-beskrivningar (typer) som återanvänds tvärs tjänstedomäner beskrives på en dedikerad [wiki-sida](https://bitbucket.org/rivta-domains/best-practice/wiki/Home).

Wikisidan uppdateras när typerna ändras, men ändringar dokumenteras i ärendelogg överst på sidan. Varje loggad ändring ges ett nytt löpnummer (1,2,3 etc) som samtidigt markerar hela wikisidans version.

Vis användning av informationskomponenterna i en tjänstekontraktsbeskrivning kopieras strukturen och beskrivningen av den aktuella typen till tjänstekontraktsbeskrivningen. 


# Header och begäran för journal- och läkemedelskontrakt (JoL-kontrakt)

I mappen schema finns xml-schema för JoL-konraktens gemensamma begäran och header. Dessa kan användas som stöd för att skapa JoL-kontraktens xml-schema.



# [Till wiki](https://bitbucket.org/rivta-domains/best-practice/wiki)
